﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using Assets.coreScripts.main.model;
using System.Collections.Generic;
using Newtonsoft.Json;

public class ConfigTools : Editor
{

    [MenuItem("ConfigTools/MakeSchemaDemo")]
    private static void MakeConfigTest()
    {
        ReelInfo info1 = new ReelInfo { ID = 501, IsBossReel = false, Speed = .8f };
        ReelInfo info2 = new ReelInfo { ID = 502, IsBossReel = true, Speed = .9f };
        List<ReelInfo> list = new List<ReelInfo> { info1, info2 };
        StageConfig stage = new StageConfig { ID = 1001, BossId = 1, ReelList = list, Boss_x = 0, Boss_y = 0,Hero_x =1,Hero_y = 1 };

        List<StageConfig> tempList = new List<StageConfig>();
        tempList.Add(stage);
        tempList.Add(stage);

        new ConfigTools().MadeConfig<StageConfig>(JsonConvert.SerializeObject(tempList, Formatting.Indented));


        MonsterConfig bpc = new MonsterConfig
        {
            ID = 51,
            HRDrag = 120.2f,
            VRDrag = -1.65f,
            UpForce = 120f,
            OriginalMass = 22.08f,
            OriginalDrag = 5f,
            OriginalID = 0,
            Min_x = -.9f,
            Min_y = -20f,
            Max_x = .9f,
            Max_y = .6f,
            AI_Frequency = 3f,
        };
        MonsterConfig bpc2 = new MonsterConfig
        {
            ID = 52,
            HRDrag = 120.2f,
            VRDrag = -1.65f,
            UpForce = 120f,
            OriginalMass = 22.08f,
            OriginalDrag = 5f,
            OriginalID = 0,
            Min_x = -.9f,
            Min_y = -20f,
            Max_x = .9f,
            Max_y = .6f,
            AI_Frequency = 3f,
        };
        List<MonsterConfig> monstConfigList = new List<MonsterConfig>();
        monstConfigList.Add(bpc);
        monstConfigList.Add(bpc2);
        new ConfigTools().MadeConfig<MonsterConfig>(JsonConvert.SerializeObject(monstConfigList, Formatting.Indented));

        HeroConfig h1 = new HeroConfig
        {
            ID = 1,
            HRDrag = 120.2f,
            VRDrag = -1.65f,
            UpForce = 120f,
            OriginalMass = 22.08f,
            OriginalDrag = 5f,
            OriginalID = 0,
            Min_x = -.9f,
            Min_y = -20f,
            Max_x = .9f,
            Max_y = .6f,
            VH_Delay =.1f,
            HR_Delay = .1f,
        };
        var l1 = new List<HeroConfig>();
        l1.Add(h1);
        new ConfigTools().MadeConfig<HeroConfig>(JsonConvert.SerializeObject(l1, Formatting.Indented));


    }

    private string _strPath;
    private StringWriter _stringWriter;

    public void MadeConfig<T>(string str) where T : class
    {
        _strPath = string.Format("{0}/Resources/Config/SchemaDemo/{1}.json", Application.dataPath, typeof(T).Name);

        if (File.Exists(_strPath)) File.Delete(_strPath);
        File.WriteAllText(_strPath, str, System.Text.Encoding.UTF8);
        Debug.Log(string.Format("File Path : {0} Create Compleate !", _strPath));
    }
}
