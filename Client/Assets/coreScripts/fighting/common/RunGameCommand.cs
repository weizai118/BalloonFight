﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.fighting.util;
using strange.extensions.command.impl;

namespace Assets.coreScripts.fighting.common
{
    public class RunGameCommand:EventCommand
    {
        [Inject]
        public IGameTimer timer { get; set; }

        public override void Execute()
        {
            //dispatcher.Dispatch(GameConfig.ReelState.START);
            //dispatcher.Dispatch(GameConfig.CoreEvent.GAME_START);

            timer.Start();
        }
    }
}
