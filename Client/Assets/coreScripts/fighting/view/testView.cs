﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.coreScripts.fighting.view
{
    public class testView:EventView
    {
        public void StartGame(GameObject o)
        {
            o.SetActive(false);
            dispatcher.Dispatch(GameConfig.PropsState.STARTGAME,1);
        }
    }
}
