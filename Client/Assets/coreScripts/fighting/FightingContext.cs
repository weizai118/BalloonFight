﻿using System;
using System.Collections.Generic;
using Assets.coreScripts.common;
using Assets.coreScripts.common.SequenceCommand;
using Assets.coreScripts.fighting.common;
using Assets.coreScripts.fighting.controller;
using Assets.coreScripts.fighting.controller.startupSequence;
using Assets.coreScripts.fighting.view;
using Assets.coreScripts.main.model;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using UnityEngine;

namespace Assets.coreScripts.fighting
{
    public class FightingContext : MVCSContext
    {
        public FightingContext(MonoBehaviour view) : base(view)
        {
        }

        public FightingContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
		{
		}

        protected override void mapBindings()
        {
            //injectionBinder.Bind<User>().ToSingleton();
            //injectionBinder.Bind<DataBaseCommon>().ToSingleton();
            //injectionBinder.Bind<IUnitAPI>().To<CustomAPI>().ToSingleton();
            mediationBinder.Bind<testView>().To<testMediator>();

            injectionBinder.Bind<FightingCommon>().ToSingleton();

            //mediationBinder.Bind<BackgroundView>().To<ReelMediator>();
            mediationBinder.Bind<ReelView>().To<ReelMediator>();
            mediationBinder.Bind<PropsView>().To<PropsMediator>();

            mediationBinder.Bind<DisplayBoardView>().To<DisplayBoardMediator>();
            mediationBinder.Bind<PlayerView>().To<PlayerMediator>();
            mediationBinder.Bind<OperationView>().To<OperationMediator>();
            mediationBinder.Bind<EnemyView>().To<EnemyMediator>();
            mediationBinder.Bind<ObstructionView>().To<ObstructionMediator>();

            commandBinder.Bind(GameConfig.CoreEvent.OBSTRUCTION_HIT).To<ObstructionCommand>();
            commandBinder.Bind(GameConfig.CoreEvent.USER_TOUCH).To<OperationCommand>();
            commandBinder.Bind(GameConfig.CoreEvent.GAME_OVER).To<GameOverCommand>();
            commandBinder.Bind(GameConfig.EnemyState.GET_HIT).To<EnemyGetHitCommand>();
            commandBinder.Bind(GameConfig.PlayerState.GET_HIT).To<PlayerGethitCommand>();
            commandBinder.Bind(GameConfig.RoleEvent.ENEMY_AI).To<EnemyAICommand>();
            commandBinder.Bind(GameConfig.PropsState.PROPS_COMMAND).To<PropsCommand>();
            //commandBinder.Bind(GameConfig.PropsState.RUNTIME).To<GameTimeStartCommand>();
            //commandBinder.Bind(GameConfig.PropsState.STOPTIME).To<GameTimeStopCommand>();

            commandBinder.Bind(GameConfig.CoreEvent.GAME_RESTART).To<GameReStartCommand>();
            //commandBinder.Bind(GameConfig.PropsState.STARTGAME).To<RunGameCommand>().Once().InSequence();

            if (this == Context.firstContext)
            {
                injectionBinder.Bind<User>().CrossContext().ToSingleton();
                injectionBinder.Bind<DataBaseCommon>().CrossContext().ToSingleton();
                injectionBinder.Bind<IUnitAPI>().To<CustomAPI>().CrossContext().ToSingleton();
                commandBinder.Bind(ContextEvent.START).To<StartAppCommand>().To<StartFightingCommand>().To<RunGameCommand>().InSequence().Once();
            }
            else 
            {
                //commandBinder.Bind(ContextEvent.START).To<InitBaseDataCommand>().To<StartFightingCommand>().Once();
            }

            

        }

    }
}
