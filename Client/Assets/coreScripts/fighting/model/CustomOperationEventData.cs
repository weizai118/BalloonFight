﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.coreScripts.fighting.model
{
    public class CustomOperationEventData : CustomEventData
    {
        public GameConfig.Direction dir { get; set; }
        public GameConfig.OperationEvent operationEventType { get; set; }
    }
}
