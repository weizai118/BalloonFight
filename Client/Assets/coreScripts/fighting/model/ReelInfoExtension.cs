﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.main.model;
using UnityEngine;

namespace Assets.coreScripts.fighting.model
{
    public class ReelInfoExtension : ReelInfo
    {
        public GameObject obj { get; set; }
        //public Vector2 initPosition { get; set; }
        public bool hasRender { get; set; }
    }
}
