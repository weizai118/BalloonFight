﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

namespace Assets.coreScripts.main.model
{
    public class BaseRolePropertiy : BaseConfig
    {
        public int HP { get; set; }
        public float UpForce { get; set; }
        public float HRDrag { get; set; }
        public float VRDrag { get; set; }

        private Vector2 minVoc;
        private Vector2 maxVoc;

        [JsonIgnore]
        public Vector2 MinVoc
        {
            get {
                if (minVoc == Vector2.zero)
                {
                    minVoc = new Vector2(Min_x,Min_y);
                }
                return minVoc;
            }
        }
        [JsonIgnore]
        public Vector2 MaxVoc
        {
            get
            {
                if (maxVoc == Vector2.zero)
                {
                    maxVoc = new Vector2(Max_x, Max_y);
                }
                return maxVoc;
            }
        }


        public float Min_x { get; set; }
        public float Min_y { get; set; }

        public float Max_x { get; set; }
        public float Max_y { get; set; }

        public int OriginalID { get; set; }
        public float OriginalMass { get; set; }
        public float OriginalDrag { get; set; }
    }
}
