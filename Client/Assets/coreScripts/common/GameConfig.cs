﻿
public class GameConfig
{
    public enum Direction
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    public enum BallonType
    {
        PLAYER,
        ENMY
    }

    public enum ObstructionModel
    {
        ON_HIT,
        NORMAL,
        //FINALLY,
        OUTREEL_DIE,
        DRAPSEA_DIE,
        GOLD,
        //PLAYER_HURT,
        BOUNCE,
        HURT_BOUNCE,
        ENTER_BOSSSTAGE,
        ENEMY_BALLON_HURT,
        PLAYER_BALLON_HURT,
    }

    public enum DisplayBoardEvent
    {
        UPDATE
    }

    public enum ReelState
    {
        START,
        STOP
    }

    public enum PropsState
    {
        STARTGAME,
        PROPS_COMMAND,
        PAUSE_RELEASE,
        WIN,
        LOSE,
        RESTART,
        OnPause,
        RUNTIME,
        STOPTIME
    }

    public enum PlayerState
    {
        NORMAL,
        DIE,
        GET_HIT,
        WINNER,
        LOUSER
    }

    public enum EnemyState
    {
        NORMAL,
        DIE,
        GET_HIT,
        WINNER,
        LOUSER
    }

    public enum EnemyLevel
    {
        NORMAL,
        BOSS,
        ELITE
    }

    public enum CoreEvent
    {
        GAME_START,
        GAME_UPDATE,
        GAME_OVER,
        GAME_RESTART,
        USER_TOUCH,
        OBSTRUCTION_HIT,
        LOADING_FIGHTING,
        PAUSE
    }

    //public enum OperationEvent
    //{
    //    UP_CLICK,
    //    DOWN_CLICK,
    //    LEFT_CLICK,
    //    RIGHT_CLICK,
    //}

    public enum RoleEvent
    {
        PLAYER_GETHIT,
        PLAYER_MOVE,
        PLAYER_CANCEL_MOVE,
        ENEMY_GETHIT,
        ENEMY_AI,
        ENEMY_PRE_AI,
        ENEMY_MOVE,
        EMEMY_CANCEL_MOVE,
        SHOW_ENEMY
    }

    public enum AIEventType
    {
        AI_LOOP,
        AI_FORCE
    }



    public enum OperationEvent
    {
        NORMAL_TOUCH,
        ONPRESS,
        ONRELEASE
    }

    public static int TestPage = 3;
    public static float BackgroundImageWidth = 13.65f;
    public static int MAX_BALLON = 3;
    public static float REEL_SPEED = 2f;
    public static bool REEL_STATE;
}