﻿//using UnityEngine;
//using System.Collections;
//using Assets.coreScripts.fighting.model;

//public class ObstructionMaterial : MonoBehaviour
//{
//    public GameConfig.ObstructionModel Model = GameConfig.ObstructionModel.BOUNCE;
//    public GameConfig.Direction Dir = GameConfig.Direction.UP;
//    private float force = 20;
//    public float CustomForce = 0;


//    /// <summary>
//    /// 气球碰撞 金币碰撞
//    /// </summary>
//    /// <param name="other"></param>
//    public void OnTriggerEnter2D(Collider2D other)
//    {
//        //coll.rigidbody.AddForce()
//        if (CustomForce > 0)
//            force = CustomForce;

//        CustomHitEventData data = new CustomHitEventData();
//        data.currentObj = gameObject;
        

//        if (other.gameObject.tag.ToUpper() == GameConfig.BallonType.PLAYER.ToString())
//        {

//            switch (Model)
//            {
//                case GameConfig.ObstructionModel.GOLD:
//                    GameObject.Destroy(gameObject);
//                    return;
//                case GameConfig.ObstructionModel.ENTER_BOSSSTAGE:
//                    CoreLogicCtrl.Instance.ShowBoss();
//                    return;
//                default:
//                    break;
//            }
//        }
//    }



//    /// <summary>
//    /// 平台碰撞 墙壁碰撞   
//    /// </summary>
//    /// <param name="coll"></param>
//    public void OnCollisionEnter2D(Collision2D coll)
//    {
//        //coll.rigidbody.AddForce()
//        if (CustomForce > 0)
//            force = CustomForce;

//        CustomHitEventData data = new CustomHitEventData();
//        data.targetObj = gameObject;


//        if (coll.gameObject.tag.ToUpper() == GameConfig.BallonType.PLAYER.ToString())
//        {

//            switch (Model)
//            {
//                case GameConfig.ObstructionModel.OUTREEL_DIE:
//                    data.hitModel = GameConfig.ObstructionModel.OUTREEL_DIE;
//                    coll.gameObject.SendMessage("OnPlayerDie", data, SendMessageOptions.RequireReceiver);
//                    return;
//                case GameConfig.ObstructionModel.DRAPSEA_DIE:
//                    data.hitModel = GameConfig.ObstructionModel.DRAPSEA_DIE;
//                    coll.gameObject.SendMessage("OnPlayerDie", data, SendMessageOptions.RequireReceiver);
//                    return;
//                case GameConfig.ObstructionModel.PLAYER_HURT:
//                    CoreLogicCtrl.Instance.AttackUser(.2f);
//                    return;
//                case GameConfig.ObstructionModel.HURT_BOUNCE:
//                    CoreLogicCtrl.Instance.AttackUser(.2f);
//                    break;
//                default:
//                    break;
//            }
//        }
//        else { return;}


//        if (!coll.rigidbody)
//            return;


//        switch (Dir)
//        {
//            case GameConfig.Direction.UP:
//                coll.rigidbody.AddForce(Vector2.up * force, ForceMode2D.Impulse);
//                break;
//            case GameConfig.Direction.DOWN:
//                coll.rigidbody.AddForce(-Vector2.up * force, ForceMode2D.Impulse);
//                break;
//            case GameConfig.Direction.LEFT:
//                coll.rigidbody.AddForce(-Vector2.right * force, ForceMode2D.Impulse);
//                break;
//            case GameConfig.Direction.RIGHT:
//                coll.rigidbody.AddForce(Vector2.right * force, ForceMode2D.Impulse);
//                break;
//            default:
//                break;
//        }

//    }
//}