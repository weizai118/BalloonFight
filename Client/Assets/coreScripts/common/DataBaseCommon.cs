﻿using System;
using System.Collections.Generic;
using Assets.coreScripts.main.model;
using System.Linq;


namespace Assets.coreScripts.common
{
    public class DataBaseCommon
    {
        [Inject]
        public IUnitAPI common { get; set; }
        public List<StageConfig> StageConfigList { get; set; }
        public List<MonsterConfig> MonsterConfigList { get; set; }
        public List<HeroConfig> HeroConfigList { get; set; }

        public void Init()
        {
            StageConfigList = common.LoadConfig<StageConfig>();
            MonsterConfigList = common.LoadConfig<MonsterConfig>();
            HeroConfigList = common.LoadConfig<HeroConfig>();
        }

        public T GetConfigByID<T>(int id,List<T> l) where T : BaseConfig
        {
            return l.Where(x => x.ID == id).SingleOrDefault();
        }
    }
}
